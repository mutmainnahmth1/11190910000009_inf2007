package id.mutmainnahmth.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class BinarySearch {

    public int getBinarySearch(int L[], int n, int x) {
        int i , j, k = 0;
        boolean ketemu;
        i = 0;
        j = n ;
        ketemu = false;

        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int[] L = {13, 14, 15, 16, 21, 76};
        int x;
        int n = 8;

        BinarySearch app = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan x: ");
        x = in.nextInt();

        System.out.println("indeks = " + app.getBinarySearch(L, n, x));
    }
}
    

