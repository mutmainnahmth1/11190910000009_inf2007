package id.mutmainnahmth.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class OtherSearch {

    public int getOtherSearch (int L[], int n,int x) {
        int i, indeks = -1;
        
        for (i = 0; i <= n - 1; i++) {
            if (L[i] == x) {
                indeks = i;
            }           
        }
        return indeks;
    }
    
    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
        int x, n = 6;
        
        Scanner in = new Scanner(System.in);
        OtherSearch app = new OtherSearch();
        
        System.out.print("masukkan x: ");
        x = in.nextInt();
        
        System.out.println("indeks = " + app.getOtherSearch(L, n, x));
    }
    
}
