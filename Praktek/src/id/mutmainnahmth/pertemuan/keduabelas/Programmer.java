package id.mutmainnahmth.pertemuan.keduabelas;

/**
 *
 * @author Mutmainnah
 */
public class Programmer extends Pegawai {

    private int bonus;
    private int tunjangan;

    public Programmer(String nama, int gaji, int tunjangan) {
        super(nama, gaji);
        this.bonus = bonus;
        this.tunjangan = tunjangan;
    }

    public int infogaji() {
        return gaji + bonus;
    }

    public int infoBonus() {
        return gaji + bonus + tunjangan;
    }

}
