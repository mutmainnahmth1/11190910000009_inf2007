package id.mutmainnahmth.pertemuan.keduabelas;

/**
 *
 * @author Mutmainnah
 */
public class TabunganTest {

    public static void main(String[] args) {
        Tabungan t = new Tabungan(5000);
        System.out.println("Saldo awal : " + t.saldo);
        t.AmbilUang(2300);
        System.out.println("Jumlah Uang yang diambil : 2300");
        System.out.println("Saldo Sekarang : " + t.saldo);
    }

}
