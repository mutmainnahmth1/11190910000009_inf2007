package id.mutmainnahmth.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class Segitiga {
    double alas, tinggi, luas;
    
    public Segitiga() {
    Scanner in = new Scanner(System.in);
       System.out.println("===Luas Segitiga===");
       System.out.println("Masukan alas: ");
       alas = in.nextDouble();
       System.out.println("Masukan tinggi: ");
       tinggi = in.nextDouble();
       luas = alas * tinggi / 2;
       System.out.println("Luas: " + luas);
    }
}
