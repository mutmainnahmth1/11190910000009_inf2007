package id.mutmainnahmth.pertemuan.ketujuh;

/**
 *
 * @author Mutmainnah
 */
public class Genap {
    public boolean getHasil(int bilangan) {
        return (bilangan % 2 == 0);
    }
}
