package id.mutmainnahmth.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class AplikasiGenap {
    public static void main(String[] args) {
        int bilangan;
        
        Scanner in = new Scanner(System.in);
        System.out.println("bilangan: ");
        bilangan = in.nextInt();
        
        Genap genap = new Genap();
        if (genap.getHasil(bilangan)) {
            System.out.println("Genap");
        } else {
            System.out.println("Ganjil");
        }    
    }
}

