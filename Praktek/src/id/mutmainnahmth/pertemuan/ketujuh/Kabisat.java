package id.mutmainnahmth.pertemuan.ketujuh;

/**
 *
 * @author Mutmainnah
 */
public class Kabisat {
    public boolean getHasil(int tahun) {
        if ((tahun % 4 == 0) && (tahun % 100 != 0) || (tahun % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }
}
