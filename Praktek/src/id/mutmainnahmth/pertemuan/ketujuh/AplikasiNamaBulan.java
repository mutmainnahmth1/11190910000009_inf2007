package id.mutmainnahmth.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class AplikasiNamaBulan {
    public static void main(String[] args) {
        int noBulan;
        
        Scanner in = new Scanner(System.in);
        System.out.println("no bulan: ");
        noBulan = in.nextInt();
        
        NamaBulan namaBulan = new NamaBulan();
        System.out.println(namaBulan.getNama(noBulan));
    }
}
