package id.mutmainnahmth.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class AplikasiSegitigaParameter {
     public static void main(String[] args) {
        int i, N; 
        double a, t;
        Scanner in = new Scanner(System.in);
        
        System.out.println("N: ");
        N = in.nextInt();
        
        for (i = 1; i <= N; i++) {
            System.out.println("masukan alas: ");
            a = in.nextDouble();
            System.out.println("masukan tinggi: ");
            t = in.nextDouble();
            SegitigaParameter segitigaParameter = new SegitigaParameter(a, t);
        }
    }
}
