package id.mutmainnahmth.pertemuan.ketujuh;

/**
 *
 * @author Mutmainnah
 */
public class SegitigaParameter {
   double luas;
   
   public SegitigaParameter(double alas, double tinggi){
       luas = (alas * tinggi) / 2;
       System.out.println("Luas = " + luas);
   }
}
