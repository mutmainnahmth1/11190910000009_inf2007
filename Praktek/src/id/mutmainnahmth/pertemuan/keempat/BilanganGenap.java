package id.mutmainnahmth.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */

public class BilanganGenap {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Input sebuah bilangan : ");
        int bilangan = s.nextInt();
        if (bilangan % 2 == 0) {
            System.out.println("Genap");
        }
    }
}
