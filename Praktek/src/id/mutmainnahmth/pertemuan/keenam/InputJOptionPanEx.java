package id.mutmainnahmth.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Mutmainnah
 */
public class InputJOptionPanEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        System.out.println("Bilangan: " + bilangan);
    }
}
