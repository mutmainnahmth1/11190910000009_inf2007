package id.mutmainnahmth.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Mutmainnah
 */
public class OutputJOptionPanEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan: " + bilangan, "Hasil Input", JOptionPane.INFORMATION_MESSAGE);
        
    }
 }