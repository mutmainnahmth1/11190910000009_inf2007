package id.mutmainnahmth.pertemuan.keenam;

/**
 *
 * @author Mutmainnah
 */
public class HandlingPembagiFinally {
    public static void main(String[] args) {
        try {
            int a = 10;
            int b  = 0;
            int c = a / b;
            
             System.out.println("Hasil:" + c);
        } catch (Throwable error) {
            System.out.println("Ups, terjadi error: ");
            System.out.println(error.getMessage());
        }   finally {
            System.out.println("pasti akan dijalankan");
        }
    }
}
