package id.mutmainnahmth.pertemuan.ketiga;

/**
 *
 * @author Mutmainnah
 */
public class IncDec {
    public static void main(String[] args) {
        int x = 8, y = 13;
        System.out.println(" x = " + x);
        System.out.println(" y = " + y);
        System.out.println(" x = " + ++x);
        System.out.println(" y = " + y++);
        System.out.println(" x = " + x--);
        System.out.println(" y = " + --y);
    }
}
