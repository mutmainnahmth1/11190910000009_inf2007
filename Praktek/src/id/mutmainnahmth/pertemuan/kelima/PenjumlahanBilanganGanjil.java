package id.mutmainnahmth.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class PenjumlahanBilanganGanjil {
    public static void main(String[] args) {
        int i, n, bilangan = 1, jumlah = 0;
        Scanner in = new Scanner(System.in);
        System.out.println( "n = " );
        n = in.nextInt();
        for (i =1; i <= n; i++) {
            jumlah = jumlah + bilangan;
            bilangan += 2;
        }
        System.out.println("jumlah = " + jumlah);
    }
}
