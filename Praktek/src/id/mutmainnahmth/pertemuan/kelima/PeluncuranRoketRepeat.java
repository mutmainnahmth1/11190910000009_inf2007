package id.mutmainnahmth.pertemuan.kelima;

/**
 *
 * @author Mutmainnah
 */
public class PeluncuranRoketRepeat {
    public static void main(String[] args) {
        int i = 100;
        
        do {
            System.out.println(i);
            i--;
        } while (i >= 0);
        System.out.println("Go");
    }
}
