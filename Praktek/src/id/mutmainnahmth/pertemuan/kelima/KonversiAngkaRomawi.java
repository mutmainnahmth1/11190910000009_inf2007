package id.mutmainnahmth.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class KonversiAngkaRomawi {
    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        int angka, i, n;
        
        System.out.print("masukan angka : ");
        angka = masukan.nextInt();
        while(angka != -1){
            if (angka <= 32767) {
                if (angka <= 0) {
                    System.out.print("angka yang dimasukan salah");
                }
                if (angka >= 1000) {
                    n = angka / 1000;
                    for (i = 1; i <= n; i++) {
                        System.out.print("M");
                    }
                    angka = angka - (1000 * n);
                }
                if (angka >= 500) {
                    if (angka >= 900) {
                        System.out.print("CM");
                        angka = angka - 900;
                    }
                    n = angka / 500;
                    for (i = 1; i <= n; i++) {
                        System.out.print("D");
                    }
                    angka = angka - (500 * n);
                }
                if (angka >= 100) {
                    if (angka >= 400) {
                        System.out.print("CD");
                        angka = angka - 400;
                    }
                    n = angka / 100;
                    for (i = 1; i <= n; i++) {
                        System.out.print("C");
                    }
                    angka = angka - (100 * n);
                }
                if (angka >= 50) {
                    if (angka >= 90) {
                        System.out.print("XC");
                        angka = angka - 90;
                    }
                    n = angka / 50;
                    for (i = 1; i <= n; i++) {
                        System.out.print("L");
                    }
                    angka = angka - (50 * n);
                }
                if (angka >= 10) {
                    if (angka >= 40) {
                        System.out.print("XL");
                        angka = angka - 40;
                    }
                    n = angka / 10;
                    for (i = 1; i <= n; i++) {
                        System.out.print("X");
                    }
                    angka = angka - (10 * n);
                }
                if (angka >= 5) {
                    if (angka == 9) {
                        System.out.print("IX");
                        angka = angka - 9;
                    } else {
                        n = angka / 5;
                        for (i = 1; i <= n; i++) {
                        System.out.print("V");
                    }
                    angka = angka - (5 * n);
                    }
                }
                if (angka >= 1) {
                    if (angka == 4) {
                        System.out.print("IV");
                        angka = angka - 4;
                    } else {
                        n = angka;
                        for (i = 1; i <= n; i++) {
                        System.out.print("I");
                    }
                }
            }
        }
        
            
        System.out.print("masukan angka: ");
        angka = masukan.nextInt();
        }
    }
}       
                
                   
