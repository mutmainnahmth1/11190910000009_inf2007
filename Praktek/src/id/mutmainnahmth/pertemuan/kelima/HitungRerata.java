package id.mutmainnahmth.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class HitungRerata {
    public static void main(String[] args) {
        int i, x, jumlah;
        float rerata;
        
        i = 0;
        jumlah = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("x = ");
        x = in.nextInt();
        
        while (x != -1) {
            i = i + 1;
            jumlah = jumlah + x;
            System.out.println("x = ");
            x = in.nextInt();
       }
       if (i != 0) {
           rerata = (float) jumlah / i;
           System.out.println("rata-rata = " + rerata);
       } else {
           System.out.println("tidak ada ujian nilai yang dimasukan");
       }
    }
}
