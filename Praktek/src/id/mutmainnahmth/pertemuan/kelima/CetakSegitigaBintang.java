package id.mutmainnahmth.pertemuan.kelima;

import java.util.Scanner;
/**
 *
 * @author Mutmainnah
 */
public class CetakSegitigaBintang {
    public static void main(String[] args) {
        int i, j, n = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("n = ");
        n = in.nextInt();
        for (i = 1; i <= n; i++){
            System.out.println("");
            for (j = 1; j <= i; j++){
                System.out.println("*");

            }
        }
        System.out.println("");
    }
}
