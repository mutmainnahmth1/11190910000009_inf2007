package id.mutmainnahmth.pertemuan.kelima;

/**
 *
 * @author Mutmainnah
 */
public class CetakBanyakHelloWorldRepeat {
    public static void main(String[] args) {
        int i = 1;
        
        do {
            System.out.println("Hello, World");
            i++;
        } while (i <= 10);
    }
}
