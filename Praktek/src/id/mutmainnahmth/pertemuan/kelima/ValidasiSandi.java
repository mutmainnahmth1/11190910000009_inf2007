package id.mutmainnahmth.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class ValidasiSandi {
    public static void main(String[] args) {
        final String password = "abc123";
        String sandi;
        boolean sah;
        int count;
        
        Scanner in = new Scanner(System.in);
        count = 1;
        sah = false;
        while ((! sah) && (count <= 3)) {
            System.out.print( "sandi = " );
            sandi = in.nextLine();
            if (password.equals(sandi)) {
                sah = true;
            } else {
                count++;               
            }
        }
    }
}
