package id.mutmainnahmth.pertemuan.kesebelas;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Mutmainnah
 */
public class RandomArsip {

    public void setTulis(String file, int position, String record) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            // move file pointer to position spesificied
            raf.seek(position);
            // writing string to RandomAccessFile
            raf.writeUTF(record);
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }

    public String getBaca(String file, int position) {
        String record = "";
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            // move file pointer to position spesificied
            raf.seek(position);
            //reading String from RandomAccessFile
            record = raf.readUTF();
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        return record;
    }

    public static void main(String[] args) {
        String berkas = "C:\\TI 19\\temp\\random.txt";
        String data = "NIM : 123 | Nama : Budi";

        RandomArsip ra = new RandomArsip();
        ra.setTulis(berkas, 1, data);
        System.out.println("Tulis Berhasil");

        String output = ra.getBaca(berkas, 1);
        System.out.println("Baca Berhasil : " + output);
    }
}
