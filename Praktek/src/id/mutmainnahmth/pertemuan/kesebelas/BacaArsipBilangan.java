package id.mutmainnahmth.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class BacaArsipBilangan {

    public static void main(String[] args) {
//        String Bil = "C:\\TI 19\\temp\\Tulis.txt";
        int x;
        try {
            BufferedReader file = new BufferedReader(new FileReader("C:\\TI 19\\temp\\Tulis.txt"));

            Scanner line = new Scanner(file);
            while (line.hasNextInt()) {
                x = line.nextInt();
                System.out.println(x);
            }
            file.close();

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }

    }
}
