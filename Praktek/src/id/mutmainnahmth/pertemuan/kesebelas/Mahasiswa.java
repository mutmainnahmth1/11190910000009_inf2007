package id.mutmainnahmth.pertemuan.kesebelas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Mutmainnah
 */
public class Mahasiswa implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nama;
    private int nim;
    private int kodemk;
    private int sks;
    private int nilai;

    public Mahasiswa() {
    }

    public Mahasiswa(String nama, int nim, int kodemk, int sks, int nilai) {
        this.nama = nama;
        this.nim = nim;
        this.kodemk = kodemk;
        this.sks = sks;
        this.nilai = nilai;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public int getKodemk() {
        return kodemk;
    }

    public void setKodemk(int kodemk) {
        this.kodemk = kodemk;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }

    @Override
    public String toString() {
        return "Mahasiswa{" + "nama=" + nama + ", nim=" + nim + ", kodemk=" + kodemk + ", sks=" + sks + ", nilai=" + nilai + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.nama);
        hash = 43 * hash + this.nim;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mahasiswa other = (Mahasiswa) obj;
        if (!Objects.equals(this.nama, other.nama)) {
            return false;
        }
        if (this.nim != other.nim) {
            return false;
        }
        return true;
    }
}
