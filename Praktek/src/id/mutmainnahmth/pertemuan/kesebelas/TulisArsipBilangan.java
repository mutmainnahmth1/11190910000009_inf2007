package id.mutmainnahmth.pertemuan.kesebelas;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.IOException;

/**
 *
 * @author Mutmainnah
 */
public class TulisArsipBilangan {

    public static void main(String[] args) {
//        String Bil = "C:\\TI 19\\temp\\Tulis.txt";
        int i, n;
        Scanner in = new Scanner(System.in);
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("C:\\TI 19\\temp\\Tulis.txt"));
            System.out.print("masukkan n: ");
            n = in.nextInt();

            for (i = 1; i <= n; i++) {
                outFile.println(i);
            }
            outFile.close();

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}
