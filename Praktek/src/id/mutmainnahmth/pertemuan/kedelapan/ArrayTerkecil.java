package id.mutmainnahmth.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Mutmainnah
 */
public class ArrayTerkecil {
    int i, min = 9999;
    public int getTerkecil(int A[], int n) {
        for (i = 0; i < A.length; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        min = min - 1;
        System.out.println(" ");
        System.out.println("elemen yang lebih kecil = " + min);
        
        return min;
    }
    public static void main(String[] args) {
        int i, n;
        
        ArrayTerkecil app = new ArrayTerkecil();
        Scanner in = new Scanner(System.in);
        
        System.out.println("N = ");
        n = in.nextInt();
        
        int A[] = new int[n];
        
        for (i = 0; i < A.length; i++) {
            System.out.println("masukan array [" + i + "): ");
            A[i] = in.nextInt();
        }
        System.out.println("\nmatriks awal");
        for (i = 0; i < A.length; i++) {
            System.out.println(A[i] + " ");
        }
        app.getTerkecil(A, n);
   } 
}
