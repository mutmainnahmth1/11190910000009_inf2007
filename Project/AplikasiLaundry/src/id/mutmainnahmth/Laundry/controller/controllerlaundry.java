package id.mutmainnahmth.Laundry.controller;

import com.google.gson.Gson;
import id.mutmainnahmth.Laundry.model.AplikasiLaundry;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Mutmainnah
 */
public class controllerlaundry {

    private static final String FILE = "C:\\Users\\Mutmainnah\\Documents\\AplikasiLaundry\\Laundry.json";
    private AplikasiLaundry laundry;
    private final Scanner in;
    private int jenisbaju;
    private String namapelanggan;
    private String alamatpelanggan;
    private String nomorhp;
    private LocalDateTime waktuTaruh;
    private LocalDateTime waktuAmbil;
    private double berat;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public controllerlaundry() {
        in = new Scanner(System.in);
        waktuAmbil = LocalDateTime.now();
        waktuTaruh = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setLaundry() {

        System.out.println("Jenis Baju 1=Katun, 2=Sutra, 3=Denim");
        System.out.print("Masukkan Jenis Baju : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Masukkan Jenis Baju : ");
        }
        jenisbaju = in.nextInt();

        System.out.println("Nama Pelanggan");
        namapelanggan = in.next();

        System.out.println("Alamat Pelanggan");
        alamatpelanggan = in.next();

        System.out.println("Nomor Hp");
        nomorhp = in.next();

        String formatWaktuTaruh = waktuTaruh.format(dateTimeFormat);
        System.out.println("Waktu Taruh : " + formatWaktuTaruh);

        System.out.println("Berat Laundry");
        berat = in.nextDouble();

        if (berat > 0) {
            biaya = new BigDecimal(berat);// biaya.add(BigDecimal.valueOf(berat))..add();
            if (jenisbaju == 1) {
                biaya = biaya.multiply(new BigDecimal(7000));
            } else if (jenisbaju == 2) {
                biaya = biaya.multiply(new BigDecimal(9000));
            } else {
                biaya = biaya.multiply(new BigDecimal(11000));
            }
        }
        laundry = new AplikasiLaundry();
        laundry.setJenisbaju(jenisbaju);
        laundry.setNamapelanggan(namapelanggan);
        laundry.setAlamatpelanggan(alamatpelanggan);
        laundry.setNomorhp(nomorhp);
        laundry.setWaktuTaruh(waktuTaruh);
        laundry.setBerat(berat);
        laundry.setBiaya(biaya);
        

        setWriteLaundry(FILE, laundry);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            menu m = new menu();
            m.getMenuAwal();
        } else {
            setLaundry();
        }
    }
    
    public List<AplikasiLaundry> getReadLaundry(String file) {
        List<AplikasiLaundry> laundrys = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                AplikasiLaundry[] ps = gson.fromJson(line, AplikasiLaundry[].class);
                laundrys.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(controllerlaundry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(controllerlaundry.class.getName()).log(Level.SEVERE, null, ex);
        }

        return laundrys;
    }

    public void setWriteLaundry(String file, AplikasiLaundry laundry) {
        Gson gson = new Gson();

        List<AplikasiLaundry> laundrys = getReadLaundry(file);
        laundrys.remove(laundry);
        laundrys.add(laundry);

        String json = gson.toJson(laundrys);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(controllerlaundry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AplikasiLaundry getSearch(String nama) {
        List<AplikasiLaundry> laundrys = getReadLaundry(FILE);

        AplikasiLaundry p = laundrys.stream()
                .filter(pp -> nama.equalsIgnoreCase(pp.getNamapelanggan()))
                .findAny()
                .orElse(null);

        return p;
    }

    public void setPembayaran() {
        System.out.print("Masukkan Nama Pelanggan : ");
        namapelanggan = in.next();

        AplikasiLaundry p = getSearch(namapelanggan);
        if (p != null) {
            
            System.out.println("Waktu Taruh : " + p.getWaktuTaruh().format(dateTimeFormat));
            System.out.println("Berat : " + p.getBerat() + " Kg");
            if (p.getJenisbaju() == 1) {
                System.out.println("Jenis Baju : Katun");
            } else if (p.getJenisbaju() == 2) {
                System.out.println("Jenis Baju : Sutra");
            } else {
                System.out.println("Jenis Baju : Denim");
            }
            System.out.println("Biaya : " + p.getBiaya() + " Rp");
            waktuAmbil = LocalDateTime.now();
            p.setWaktuAmbil(waktuAmbil);
            System.out.println("Waktu Ambil : " + p.getWaktuAmbil().format(dateTimeFormat));
            p.setWaktuAmbil(waktuAmbil);
             p.setKeluar(true);
            
            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                   
                    setWriteLaundry(FILE, p);
                    break;
                case 2:
                    setPembayaran();
                    break;
                default:
                    menu m = new menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                menu m = new menu();
                m.getMenuAwal();
            } else {
                setPembayaran();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setPembayaran();
        }
    }

    public void getDataLaundry() {
        List<AplikasiLaundry> laundrys = getReadLaundry(FILE);
        Predicate<AplikasiLaundry> isKeluar = e -> e.isKeluar() == true;
        Predicate<AplikasiLaundry> isDate = e -> e.getWaktuTaruh().toLocalDate().equals(LocalDate.now());

        List<AplikasiLaundry> pResults = laundrys.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(AplikasiLaundry::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama Pelanggan \tJenis Baju \tWaktu Taruh \t\tWaktu Ambil \t\tBiaya");
        System.out.println("----------- \t------- \t------------------- \t------------------- \t--------");
        pResults.forEach((p) -> {
            
            if (p.getJenisbaju() == 1) {
                System.out.println(p.getNamapelanggan() + "\t\t" + "Katun" + "\t\t" + p.getWaktuTaruh().format(dateTimeFormat) + "\t" + p.getWaktuAmbil().format(dateTimeFormat) + "\t" + p.getBiaya());
            } else if (p.getJenisbaju() == 2) {
                System.out.println(p.getNamapelanggan() + "\t\t" + "Sutra" + "\t\t" + p.getWaktuTaruh().format(dateTimeFormat) + "\t" + p.getWaktuAmbil().format(dateTimeFormat) + "\t" + p.getBiaya());
            } else {
                System.out.println(p.getNamapelanggan() + "\t\t" + "Denim" + "\t\t" + p.getWaktuTaruh().format(dateTimeFormat) + "\t" + p.getWaktuAmbil().format(dateTimeFormat) + "\t" + p.getBiaya());
            }
            });
        System.out.println("----------- \t------- \t------------------- \t------------------- \t--------");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            menu m = new menu();
            m.getMenuAwal();
        } else {
            getDataLaundry();
        }
    }
}
