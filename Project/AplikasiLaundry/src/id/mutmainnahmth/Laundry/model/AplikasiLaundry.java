/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.mutmainnahmth.Laundry.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author Mutmainnah
 */
public class AplikasiLaundry implements Serializable{
    private static final long serialVersionUID = -6756463875294313469L;

    private int jenisbaju;
    private String namapelanggan;
    private String alamatpelanggan;
    private String nomorhp;
    private LocalDateTime waktuTaruh;
    private LocalDateTime waktuAmbil;
    private double berat;
    private BigDecimal biaya;
    private boolean keluar = false;
    public AplikasiLaundry(){
        
    }

    public AplikasiLaundry(int jenisbaju, String namapelanggan, String alamatpelanggan, String nomorhp, LocalDateTime waktuTaruh, LocalDateTime waktuAmbil, double berat, BigDecimal biaya) {
        this.jenisbaju = jenisbaju;
        this.namapelanggan = namapelanggan;
        this.alamatpelanggan = alamatpelanggan;
        this.nomorhp = nomorhp;
        this.waktuTaruh = waktuTaruh;
        this.waktuAmbil = waktuAmbil;
        this.berat = berat;
        this.biaya = biaya;
    }

    public int getJenisbaju() {
        return jenisbaju;
    }

    public void setJenisbaju(int jenisbaju) {
        this.jenisbaju = jenisbaju;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public void setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
    }

    public String getAlamatpelanggan() {
        return alamatpelanggan;
    }

    public void setAlamatpelanggan(String alamatpelanggan) {
        this.alamatpelanggan = alamatpelanggan;
    }

    public String getNomorhp() {
        return nomorhp;
    }

    public void setNomorhp(String nomorhp) {
        this.nomorhp = nomorhp;
    }

    public LocalDateTime getWaktuTaruh() {
        return waktuTaruh;
    }

    public void setWaktuTaruh(LocalDateTime waktuTaruh) {
        this.waktuTaruh = waktuTaruh;
    }

    public LocalDateTime getWaktuAmbil() {
        return waktuAmbil;
    }

    public void setWaktuAmbil(LocalDateTime waktuAmbil) {
        this.waktuAmbil = waktuAmbil;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }
    
    @Override
    public String toString() {
        return "laundry{" + "jenis=" + jenisbaju + ", nama=" + namapelanggan + ", alamat=" + alamatpelanggan + ", nohp=" + nomorhp +", waktu taruh=" + waktuTaruh + ", waktu ambil=" + waktuAmbil +", berat=" + berat +", biaya=" + biaya + ", keluar=" + keluar + '}';
    }
}
